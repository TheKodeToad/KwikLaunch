// Uses code modified from minecraft-launcher-core and msmc.

/*
 * minecraft-launcher-core
 *
 * MIT License
 * 
 * Copyright (c) 2019 Pierce Harriz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * MSMC
 *
 * MIT License
 * 
 * Copyright (c) 2021 Hanro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

const { Client, Authenticator } = require("minecraft-launcher-core");
const launcher = new Client();
const msmc = require("msmc");
const fetch = require("node-fetch");
const fs = require("fs");
const YAML = require("yaml");
const arg = require("arg");
const prompt = require("prompt-sync")();
const process = require("process");
const prefix = "[KwikLaunch] ";

console.log(prefix + "=============== Credits ===============");
console.log(prefix + " Created by TheKodeToad/mcblueparrot. ");
console.log(prefix + " Uses MinecraftLauncherCore by Pierce01 ");
console.log(prefix + " Uses MSMC by Hanro50 ");
console.log(prefix + "=======================================");
console.log();

var config = {
	directory: "./minecraft",
	version: "1.19",
	jvmArgs: [],
	memory: {
		min: "1G",
		max: "2G"
	}
};
var configFile = "config.yaml";

if(fs.existsSync(configFile)) {
	config = YAML.parse(fs.readFileSync(configFile, "UTF-8"));
}
else {
	fs.writeFileSync(configFile, YAML.stringify(config));
}

const args = arg({
	"--login": Boolean,
	"--directory": String,
	"--version": String,
	"--min-memory": String,
	"--max-memory": String,
	"--init": Boolean,
	"--java-path": String
});

if(args["--directory"]) {
	config.directory = args["--directory"];
}

if(args["--version"]) {
	config.version = args["--version"];
}

if(args["--min-memory"]) {
	config.memory.min = args["--min-memory"];
}

if(args["--max-memory"]) {
	config.memory.max = args["--max-memory"];
}

if(args["--java-path"]) {
	config.javaPath = args["--java-path"];
}

if(args["--init"]) {
	return;
}

function saveAccount() {
	fs.writeFileSync(accountFile, JSON.stringify(account));
}

var account = null;
var accountFile = "account.json";

if(fs.existsSync(accountFile)) {
	account = JSON.parse(fs.readFileSync(accountFile, "UTF-8"));
}

async function logIn() {
	var type;
		while(type !== "microsoft" && type !== "mojang") {
		type = prompt(prefix + "Account Type (Microsoft/Mojang): ").toLowerCase();
	}

	if(type === "microsoft") {
		// msmc's testing enviroment sometimes runs into this issue that it can't load node fetch
		msmc.setFetch(fetch)
		msmc.fastLaunch("raw",
			(update) => {
				// A hook for catching loading bar events and errors, standard with MSMC
				console.log("[MSMC]: " + update.data);
			}).then((result) => {
				if(msmc.errorCheck(result)) {
					console.error(prefix + "Could not log in: " + result.type);
					process.exit(1);
				}
				
				account = msmc.getMCLC().getAuth(result);
				account.type = "msa";
				saveAccount();
				launch();
			}).catch((reason) => {
				// If the login fails
				console.error(prefix + "Could not log in: " + reason);
				process.exit(1);
			});
	}
	else {
		var validLogin = false;
		while(!validLogin) {
			var username = prompt(prefix + "Email/Username: ");
			var password = prompt.hide(prefix + "Password: ");
			try {
				account = await Authenticator.getAuth(username, password);
				account.type = "mojang";
				saveAccount();
				launch();
				validLogin = true;
			}
			catch(error) {
				console.error(error.message);
			}
			if(!validLogin) {
				var tryAgain = prompt("Try again? ").toLowerCase();
				if(tryAgain !== "y" && tryAgain !== "yes") {
					process.exit(0);
				}
			}
		}
	}
}

if(account == null || args["--login"]) {
	logIn();
}
else {
	(async() => {
		if(account.type === "mojang") {
			try {
				console.log(prefix + "Checking login...");
				await Authenticator.validate(account.access_token, account.client_token);
			}
			catch(error) {
				try {
					console.log(prefix + "Refreshing login...");
					account = await Authenticator.refreshAuth(account.access_token, account.client_token);
					account.type = "mojang";
					saveAccount();
				}
				catch(error2) {
					console.error(prefix + "Could not refresh login");
					logIn();
					return;
				}
			}
		}
		else if(account.type === "msa") {
			if(!msmc.validate(account)) {
				account = msmc.getMCLC().getAuth(await msmc.refresh(account, (update) => {
					if(update.data) {
						console.log("[MSMC]: " + update.data);
					}
				}));
				account.type = "msa";
				saveAccount();
			}
		}
		launch();
	})();
}

function launch() {
	var opts = {
		clientPackage: null,

		authorization: account,
		root: config.directory,
		version: {
			number: config.version,
			type: "release"
		},
		customArgs: config.jvmArgs,
		memory: config.memory,
		javaPath: config.javaPath
	};

	launcher.launch(opts);

	launcher.on("debug", (e) => {
		if(e.startsWith("[MCLC]: ")) {
			e = e.substring(8);
		}
		console.log(prefix + e);
	});
	launcher.on("data", (e) => {
		console.log("[Game] " + e);
	});
}
