# KwikLaunch
Launch Minecraft without the launcher from the terminal.

# How to use

First, install [Node.js](https://nodejs.org/en/).

Next, [download this repository](https://gitlab.com/TheKodeToad/KwikLaunch/-/archive/main/KwikLaunch-main.zip), and unzip it.

In the folder, run:
```sh
npm i
node index.js --init
```

This will create config.yaml:
```yaml
# The folder to run Minecraft in.
directory: ./minecraft

# The version of Minecraft to run.
version: 1.19

# Optional: JVM Arguments.
jvmArgs:
- argument
- another argument
- I have a lot of arguments.

# Optional: Memory settings
memory:
  min: 1G
  max: 2G

# Optional: Java Path.
javaPath: /usr/lib/jvm/java-16-openjdk-amd64/bin/java
```

Run the game:
```sh
node index.js
```
On first run, this will allow you to log in.


To force a log in (if you have multiple accounts):
```sh
node index.js --login
```
